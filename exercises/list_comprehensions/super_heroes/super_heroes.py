#!/bin/env python
# Instructions: Create an array and iter over it, print only str items (Super Heroes) and concat the names with ',' until the last one concat with 'And' + 'Are Super Heroes'  
# Use the fewest possible lines of code 
mylist = [x for x in filter(lambda x: type(x) is str,["SpiderMan",1,['a','e'],{'The Joker','Deadpool'},'BatMan',(10.0,843902),'IronMan'])]
print 'Result: '+', '.join(mylist[:-1])+' And '+mylist[-1]+' Are Super Heoresnames '
