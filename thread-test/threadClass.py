import threading
import datetime
# This examples shows how to create threads from custom class extending threading.Thread

class ThreadClass(threading.Thread):
  def __init__(self, num):
    threading.Thread.__init__(self)
    self.num = num

  def run(self):
    now = datetime.datetime.now()
    print "%s says Hello World at time: %s" % (self.getName(), now)

print "I'm the main thread"
for i in range(0,10):
    t = ThreadClass(i)
    t.start()
    t.join()