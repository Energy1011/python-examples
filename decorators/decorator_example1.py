#!/bin/python

def my_decorator(f):
    def  show_name(*args):
        print "Function called:",f.__name__,":",f.__doc__
        res = f(*args)
        return res
    return show_name

@my_decorator
def message(msg):
    """ Show the message as argument"""
    print msg


message("Hello, this a decorated")