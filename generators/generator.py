#!/bin/env python

# Generators functions allow you to declare a function that behaves like an iterator, i.e. it can be used in a for loop.
def my_generator(n, m, s):
	while(n <= m):
		yield n
		n += s

x = my_generator(0, 5, 1)

#Generator created at 0x......... in memory 
print x

#Using the generator to iter over it
for n in x:
	print n 
