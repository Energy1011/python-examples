#!/bin/env python

import socket

s = socket.socket()
s.bind(("localhost", 9996))
s.listen(1)

sc, addr = s.accept()

while True:
		received = sc.recv(1024)
		if received == "quit":
			break
		print "Received:", received
		sc.send(received)

print "Good bye."
sc.close()
s.close()